import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let damage;
    const playerFirst = new Set();
    const playerSecond = new Set();
    const leftPlayerState = {
      isBlock: false,
      damageCritical: true,
      healthFighter: firstFighter.health,
      isAttack: false
    };
    const rightPlayerState = {
      isBlock: false,
      damageCritical: true,
      healthFighter: secondFighter.health,
      isAttack: false
    };

    function playerHasControl(player, criticalHit) {
      return player.has(criticalHit)
        ? player.add(event.code)
        : player.clear();
    }

    document.addEventListener("keydown", (event) => {
      if (firstFighter.health <= 0) resolve(secondFighter);
      if (secondFighter.health <= 0) resolve(firstFighter);
      console.log(leftPlayerState.isBlock);
      // console.log(rightPlayerState.isBlock);
      switch (event.code) {
        case controls.PlayerOneAttack:
          if (leftPlayerState.isBlock) break;
          damage = rightPlayerState.isBlock ? 0 : getDamage(firstFighter, secondFighter);
          leftPlayerState.isAttack = true;
          secondFighter.health -= damage;
          if (secondFighter.health <= 0) resolve(firstFighter);
          secondFighter.health = secondFighter.health <= 0 ? 0 : secondFighter.health;
          break;
        case controls.PlayerTwoAttack:
          if (rightPlayerState.isBlock) break;
          damage = leftPlayerState.isBlock ? 0 : getDamage(secondFighter, firstFighter);
          leftPlayerState.isAttack = true;
          firstFighter.health -= damage;
          if (firstFighter.health <= 0) resolve(secondFighter);
          firstFighter.health = firstFighter.health <= 0 ? 0 : firstFighter.health;
          break;
        case controls.PlayerOneBlock:
          leftPlayerState.isBlock == false ? leftPlayerState.isBlock = true : leftPlayerState.isBlock = false;
          break;
        case controls.PlayerTwoBlock:
          rightPlayerState.isBlock == false ? rightPlayerState.isBlock = true : rightPlayerState.isBlock = false;
          break;
      }
      healthChange("left", firstFighter, leftPlayerState);
      healthChange("right", secondFighter, rightPlayerState);
    });
    document.addEventListener("keydown", (event) => {
      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      }
      else if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }
      if (event.code === controls.PlayerOneCriticalHitCombination[0] && leftPlayerState.damageCritical) {
        playerFirst.clear();
        playerFirst.add(event.code)
      }
      if (event.code === controls.PlayerOneCriticalHitCombination[1]) {
        playerHasControl(playerFirst, controls.PlayerOneCriticalHitCombination[0]);
      }
      if (event.code === controls.PlayerOneCriticalHitCombination[2]) {
        if (playerFirst.has(controls.PlayerOneCriticalHitCombination[1])) {
          secondFighter.health -= firstFighter.attack * 2;
          secondFighter.health = secondFighter.health <= 0 ? 0 : secondFighter.health;

          leftPlayerState.isBlock = false;

          leftPlayerState.damageCritical = false;
          if (secondFighter.health <= 0) resolve(firstFighter);
          setTimeout(() => {
            leftPlayerState.damageCritical = true
          }, 10000);
        }
        playerFirst.clear();
      }
      if (event.code === controls.PlayerTwoCriticalHitCombination[0] && rightPlayerState.damageCritical) {
        playerSecond.clear();
        playerSecond.add(event.code)
      }
      if (event.code === controls.PlayerTwoCriticalHitCombination[1]) {
        playerHasControl(playerSecond, controls.PlayerTwoCriticalHitCombination[0]);
      }
      if (event.code === controls.PlayerTwoCriticalHitCombination[2]) {
        if (playerSecond.has(controls.PlayerTwoCriticalHitCombination[1])) {
          firstFighter.health -= secondFighter.attack * 2;
          firstFighter.health = firstFighter.health <= 0 ? 0 : firstFighter.health;

          rightPlayerState.isBlock = false;

          rightPlayerState.damageCritical = false;
          if (firstFighter.health <= 0) {
            resolve(secondFighter);
          }
          setTimeout(() => {
            rightPlayerState.damageCritical = true
          }, 10000);
        }
        playerSecond.clear();
      }
      switch (event.code) {
        case controls.PlayerOneBlock:
          playerFirst.clear();
          if (leftPlayerState.isBlock) {
            break;
          }
          leftPlayerState.isBlock = true;
          break;
        case controls.PlayerTwoBlock:
          playerSecond.clear();
          if (rightPlayerState.isBlock) {
            break;
          }
          rightPlayerState.isBlock = true;
          break;
        case controls.PlayerTwoAttack:
          playerSecond.clear();
          break;
        case controls.PlayerOneAttack:
          playerFirst.clear();
          break;
      }
      healthChange("left", firstFighter, leftPlayerState);
      healthChange("right", secondFighter, rightPlayerState);
    });
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);

  return Math.max(0, damage);
  // return damage
}

export function getHitPower(fighter) {
  return fighter.attack * getRandomInt();

  // return hit power
}

export function getBlockPower(fighter) {
  return fighter.defense * getRandomInt();

  // return block power
}

function getRandomInt() {
  return Math.random() + 1;
}

function healthChange(where, { health }, { healthFighter }) {
  document.getElementById(`${where}-fighter-indicator`).style.width = `${health * 100 / healthFighter}%`
}